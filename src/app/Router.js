import { createRouter, createWebHashHistory } from 'vue-router'
import railwayPage from '../pages/railwayPage/railwayPage.vue'
import servicesPage from '../pages/servicesPage/servicesPage.vue'
import storagePage from '../pages/storagePage/storagePage.vue'
import customsPage from '../pages/customsPage/customsPage.vue'
import headerVue from '../widgets/header/header.vue'


export default createRouter({
    history: createWebHashHistory(),
    routes: [
        {path: '/', component: headerVue},
        {path: '/services', component: servicesPage},
        {path: '/services/railway', component: railwayPage},
        {path: '/services/storage', component: storagePage},
        {path: '/services/customs', component: customsPage}
    ]})