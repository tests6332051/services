/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.vue",
  ],
  theme: {
    extend: {},
    fontFamily: {
      'raleway': ['Raleway'],
      'open': ['Open Sans']
    }
  },
  plugins: [],
}

